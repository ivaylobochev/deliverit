import pool from "./pool.js";

/**
 * GET ALL SHIPMENTS
 * @returns all shipments and their full info
 */
const getAllShipments = async () => {
  const sql = `
        SELECT * 
        FROM shipments
        `;

  return await pool.query(sql);
};

/**
 * GET SHIPMENT BY ID
 * @param {number} id
 * @returns
 */
const getShipmentById = async (id) => {
  const sql = `
        SELECT * 
        FROM shipments
        WHERE idshipments = ?
        `;

  const result = await pool.query(sql, [id]);

  if (!result) {
    return null;
  }

  return result;
};

/**
 * GET SHIPMENT STATUS BY ID
 * @param {number} id
 * @returns
 */
const getShipmentStatusById = async (id) => {
  const sql = `
        SELECT status 
        FROM shipments
        WHERE idshipments = ?
        `;

  const result = await pool.query(sql, [id]);

  if (!result) {
    return null;
  }

  return result[0].status;
};

/**
 * GET SHIPMENTS FROM WAREHOUSE
 * @param {number} id
 * @returns all shipments starting from selected warehouse
 */
const getShipmentsFromWarehouse = async (id) => {
  const sql = `
    SELECT * 
    FROM shipments
    WHERE from_idwarehouses = ?
    `;

  const result = await pool.query(sql, [id]);

  if (!result) {
    return null;
  }

  return result;
};

/**
 * GET SHIPMENTS TO WAREHOUSE
 * @param {number} id
 * @returns all shipments that will be delivered to selected warehouse
 */
const getShipmentsToWarehouse = async (id) => {
  const sql = `
    SELECT * 
    FROM shipments
    WHERE to_idwarehouses = ?
    `;

  const result = await pool.query(sql, [id]);

  if (!result) {
    return null;
  }

  return result;
};

/**
 * GET ALL SHIPMENTS WITH STATUS
 * @param {string} status
 * @returns
 */
const getShipmentsByStatus = async (status) => {
  const sql = `
        SELECT *
        FROM shipments
        WHERE status = ?
        `;

  const result = await pool.query(sql, [status]);

  return result;
};

/**
 * UPDATE SHIPMENT BY ID
 * @param {number} shipmentId
 * @param {object} newData
 * @returns
 */
const updateShipmentbyId = async (shipmentId, newData) => {
  const sqlQuery = `
        UPDATE shipments  
        SET status = ?, from_idwarehouses = ?, to_idwarehouses = ?, 
        departure = ?, arrival = ?, is_deleted = ?
        WHERE idshipments = ?
        `;

  const updatedUser = await pool.query(sqlQuery, [
    newData.status,
    newData.from_idwarehouses,
    newData.to_idwarehouses,
    newData.departure,
    newData.arrival,
    newData.is_deleted,
    shipmentId,
  ]);

  const result = await getShipmentById(shipmentId);

  return !(result.length === 0) ? result : null;
};

/**
 * CREATE SHIPMENT
 * @param {object} shipment
 * @returns
 */
const createShipment = async (shipment) => {
  const sql = `
      INSERT INTO shipments (from_idwarehouses, to_idwarehouses, departure, arrival)
      VALUES (?, ?, ?, ?)
      `;
  return await pool.query(sql, [
    shipment.from_idwarehouses,
    shipment.to_idwarehouses,
    shipment.departure,
    shipment.arrival,
  ]);
};

//GET SHIPMENT WEIGHT

const getShipmentWeight = async (idshipments) => {
  const sql = `
        SELECT shipment_weight
        FROM shipments
        WHERE idshipments=?
        `;

  const result = await pool.query(sql, [idshipments]);

  console.log(result[0].shipment_weight);
  console.log(result)

  return (result[0].shipment_weight);
};


const updateShipmentWeightById = async (shipmentId, newWeight) => {
  console.log("New weight",newWeight)
  const sqlQuery = `
        UPDATE shipments  
        SET shipment_weight = ?
        WHERE idshipments = ?
        `;

  const updatedUser = await pool.query(sqlQuery, [
    newWeight,
    shipmentId,
  ]);

  const result = await getShipmentById(shipmentId);

  return !(result.length === 0) ? result : null;
};

export default {
  getAllShipments,
  getShipmentById,
  getShipmentStatusById,
  getShipmentsFromWarehouse,
  getShipmentsToWarehouse,
  getShipmentsByStatus,
  updateShipmentbyId,
  createShipment,
  getShipmentWeight,
  updateShipmentWeightById,
};
