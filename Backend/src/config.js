export const DB_CONFIG = {
  host: "localhost",
  port: "3306",
  user: "root",
  password: "",
  database: "deliver",
};

export const PORT = 3000;

export const SECRET_KEY = "sekreten_chasten_klu4";

// 60 mins * 60 secs
export const TOKEN_LIFETIME = 60 * 60;

export const DEFAULT_USER_ROLE = "User";
