import countriesData from "../data/countries-data.js";
import countries from "../data/countries-data.js";
import countriesErrors from "../errors/countries-errors.js";

/**
 * GET ALL COUNTRIES
 * @returns all countries
 */
const getCountries = async () => {
  return await countries.getAllCountries();
};

/**
 * GET COUNTRY BY NAME
 * @param {string} name
 */
const getCountryByName = async (name) => {
  // console.log(name);
  const result = await countries.getSingleCountryByName(name);
  console.log(result);

  // !result ? {data: result} : {error: countriesErrors.COUNTRY_NOT_FOUND};

  if (!result) {
    return {
      error: countriesErrors.COUNTRY_NOT_FOUND,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * GET ACTIVE COUNTRIES
 * @returns all non deleted countries
 */
const getActiveCountries = async () => {
  return await countries.getAllActiveCountries();
};

/**
 * CREATE COUNTRY
 * @param {string} name
 * @returns created country
 */
const createCountry = async (name) => {
  if (await countriesData.checkCountryExists(name)) {
    return {
      error: countriesErrors.ALREADY_EXISTS,
      data: null,
    };
  }

  const createdCountry = await countriesData.createNewCountry(name);

  return {
    error: null,
    data: createdCountry,
  };
};

export default {
  getCountries,
  getCountryByName,
  getActiveCountries,
  createCountry,
};
