import { check } from 'express-validator';

export const loginValidatorSchema = [
    check("email")
    .isEmail()
    .withMessage("Invalid email"),

    check("password")
    .isLength({
        min:5,
        max:20
    })
    .withMessage("Password must be between 5 & 20 characters!")
]