import { createContext, useContext, useEffect, useState } from 'react';
import decode from 'jwt-decode'

export const AuthContext = createContext({
  user: null,
  setUser: () => {},
});

// export const useAuthContext = () => useContext(AuthContext);

const AuthContextProvider = ({children}) => {
  const [user, setUser] = useState();

  //Optional chaining
  useEffect(() => { 
    
    if(localStorage?.getItem('token')) {
      try {
              return setUser(decode(localStorage?.getItem('token')));
      } catch(error) {
        console.error("TOKEN ERROR" , error)
      }
    } else {
      
    }
  },[])

  const setUserInfo = (token) => {

      setUser(token);
    }
  

  return (
    <AuthContext.Provider value={{ user, setUser: setUserInfo }}>
      {children}
    </AuthContext.Provider>
  )
}
export default AuthContextProvider;
