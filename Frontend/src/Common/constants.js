// import EditWarehouseDialog from "../Components/EditWarehouseDialog/EditWarehouseDialog";

export const API_URL = 'http://localhost:3000/';

export const emailErrorMessage = "Email must be between 5 and 20 characters";

export const lengthErrorMessage = "Name must be between 2 and 20 characters";

export const passwordErrorMessage = "Password must be between 5 and 20 characters";

export const successfulRegistration = "Congrats! You have registered successfully!";

export const invalidUserRole = "Role must be either 1 or 0"

export const maxShipmentWeight = 100;

export const HEADERS = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
}


export const getHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
    }
}

export const UserRole = {
    CUSTOMER: 0,
    EMPLOYEE: 1
}

// export const colWarehouses = [
//     {
//       field: "",
//       headerName: "",
//       editable: false,
//       sortable: false,
//       width: 120,
//       disableClickEventBubbling: true,
//       renderCell: (params) => {
//         return <EditWarehouseDialog {...params.row} />;
//       },
//     },
//     { field: "id", headerName: "ID", width: 100 },
//     {
//       field: "country",
//       headerName: "Country",
//       width: 150,
//       editable: true,
//     },
//     {
//       field: "city",
//       headerName: "City",
//       width: 150,
//       editable: true,
//     },
//     {
//       field: "address",
//       headerName: "Address",
//       width: 150,
//       editable: true,
//     },
//   ];