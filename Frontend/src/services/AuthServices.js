import axiosInstance from "../axios";
import { successfulRegistration } from "../Common/constants";

export const registerUser = async (credentials) => {
    const { status } = await axiosInstance.post("/users/register", credentials);
    if (status === 201) {
      return successfulRegistration;
    }
};

export const logUser = async (credentials) => {
  try {

    const res = await axiosInstance.post("/users/login", credentials);
    const { token } = res.data;
        return token
  } catch (e) {
    return e;
  }


    // localStorage.setItem('token', data.token);
}

// const loginFunc = () => {
//     loginToServer(userName, userPassword)
//     .then(result => {
//       if (result.message) {
//         updateHaveError(result.message.split(';'));
//         return;
//       }

//       const checkToken = getUserDataFromToken(result.token);

//       if (checkToken) {
//         updateHaveError(false);
//         setUser(checkToken);
//         localStorage.setItem('token', result.token);
//         setShowLogin('none');
//         history.push('/');
//       }
//     });
//   }