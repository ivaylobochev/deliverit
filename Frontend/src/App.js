import "./App.css";
import * as React from "react";
import ButtonAppBar from "./Components/NavBar/NavBar";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import DataTableParcels from "./Components/DataTableComponents/DataTableParcels";
import DataTableUsers from "./Components/DataTableComponents/DataTableUsers";
import DataTableWarehouses from "./Components/DataTableComponents/DataTableWarehouses";
import DataTableShipments from "./Components/DataTableComponents/DataTableShipments";
import Login from "./Components/Login/Login";
import RegisterPage from "./pages/RegisterPage";
import DataTableParcelsCustomer from "./Components/DataTableComponents/DataTableParcelsCustomer";


import { AuthContext } from "./Context/AuthContext";
import DataTableWarehousesCustomers from "./Components/DataTableComponents/DataTableWarehousesCustomers";

function renderLoginSwitch() {
  return (
    <Switch>
      <Route exact path="/users/login" component={Login} />
      <Route exact path="/users/register" component={RegisterPage} />
      <Route exact path="/warehouses" component={DataTableWarehousesCustomers} />
      <Route path="/">
        <Redirect to="/users/login"/>
      </Route> 
    </Switch>
  )
}

function renderCustomerSwitch() {
  return (
    <Switch>

      <Route exact path="/parcels_user" component={DataTableParcelsCustomer} />
      <Route exact path="/warehouses" component={DataTableWarehousesCustomers} />
      <Route path="/">
        <Redirect to="/warehouses"/>
      </Route> 
      
    </Switch>
  )

}

function renderEmployeeSwitch() {
  return (
    <Switch>
    <Route exact path="/warehouses" component={DataTableWarehouses} />
    <Route
            exact
            path="/users" component={DataTableUsers}
          />
          <Route
            exact
            path="/parcels"
            component={DataTableParcels}
          />
          <Route
            exact
            path="/shipments"
            component={DataTableShipments}
          />
      <Route path="/">
        <Redirect to="/warehouses"/>
      </Route> 
    </Switch>
  )

}

function App() {
  // const [user, setUser] = useState(getUserDataFromToken(localStorage.getItem('token')));
  // const { user, setUser } = useAuthContext();

  // const authEmployee = () => {
  //   if (!user || !user.role) return false;
  //   return !!user.role;
  // };

  // React.useEffect(() => {
  //   const token = localStorage.getItem("token");
  //   const decodedUserData = getUserDataFromToken(token);
  //   if (token) setUser(token);
  // }, []);

  return (
    <AuthContext.Consumer>{ authContext => (
      <BrowserRouter>
      {/* <AuthContext.Provider> */}
      <ButtonAppBar />
      {/* <BasicButtonGroup /> */}
      { !authContext.user 
          ? (renderLoginSwitch()) 
          : (!authContext.user.role 
              ? renderCustomerSwitch()
              : renderEmployeeSwitch())}
      {/* </AuthContext.Provider> */}
    </BrowserRouter>
    )}
    </AuthContext.Consumer>
  );
}

export default App;
