import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

import List from "@material-ui/core/List";

import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import { TextField } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

export default function RegisterDialog() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Button color="white" onClick={handleClickOpen}>
        Register
      </Button>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              Register
            </Typography>
            <Button autoFocus color="inherit" onClick={handleClose}>
              Register
            </Button>
          </Toolbar>
        </AppBar>
        <List>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField id="outlined-basic" label="Email" variant="outlined" />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              id="outlined-basic"
              label="Password"
              variant="outlined"
            />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              id="outlined-basic"
              label="Confirm Password"
              variant="outlined"
            />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField id="outlined-basic" label="Country" variant="outlined" />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField id="outlined-basic" label="City" variant="outlined" />
          </form>
          <br />
          <form className={classes.root} noValidate autoComplete="off">
            <TextField id="outlined-basic" label="Address" variant="outlined" />
          </form>
        </List>
      </Dialog>
    </div>
  );
}
