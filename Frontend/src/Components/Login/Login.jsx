import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { getUserDataFromToken } from "../../Common/token_functions";
import { AuthContext } from "../../Context/AuthContext";
import { logUser } from "../../Requests/user_requests";
import { Button, TextField } from "@material-ui/core";

export const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  // const [redirect, setRedirect] = useState(false);



  const submit = async (event, authContext) => {
    event.preventDefault();

      await logUser({
      email,
      password,
    }).then(token => {
      console.log(token)
      localStorage.setItem('token', token);
      authContext.setUser(getUserDataFromToken(token));
    })


    // setRedirect(true);
  };

  // if (redirect) {
  //   return <Redirect to="/" />;
  // }

  return (
    <AuthContext.Consumer>{authContext => (
<form onSubmit={e => submit(e, authContext)}>
      <h1>Please sign in</h1>
      <TextField
        variant="outlined"
        type="email"
        className="form-control"
        placeholder="Email address"
        required
        onChange={(e) => setEmail(e.target.value)}
      />
      <br/>

      <TextField
        variant="outlined"
        type="password"
        className="form-control"
        placeholder="Password"
        required
        onChange={(e) => setPassword(e.target.value)}
      />
      <br/>
      <br/>

      <Button type="submit"
                style={{ height: 52, width: 100 }}

                variant="contained"
                color="primary"
      >
        Sign in
      </Button>
    </form>
    )}</AuthContext.Consumer>
    
  );
};

export default Login;