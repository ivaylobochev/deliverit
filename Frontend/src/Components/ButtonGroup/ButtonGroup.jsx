import React from "react";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
}));

export default function BasicButtonGroup() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <ButtonGroup
        variant="contained"
        color="primary"
        aria-label="contained primary button group"
      >
        <a href="/"><Button>Users</Button></a>
        <a href="/shipments"><Button>Shipments</Button></a>
        <a href="/parcels"><Button>Parcels</Button></a>
        <a href="/warehouses"><Button>Warehouses</Button></a>
      </ButtonGroup>
    </div>
  );
}
