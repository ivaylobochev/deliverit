import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { Button, TextField } from "@material-ui/core";
import { createUser } from "../../Requests/user_requests";
import { useState } from "react";
import * as constants from "../../Common/constants";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));
const formConfig = [
  {
    key: "first_name",
    placeholder: "First name",
    type: "text",
    label: "First name",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined", 
  },
  {
    key: "last_name",
    placeholder: "Last name",
    type: "text",
    label: "Last name",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "role",
    placeholder: "role",
    type: "text",
    label: "Role",
    validations: {
      validateCallback: (value) => value !== 1 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "email",
    placeholder: "Email",
    type: "text",
    label: "Email",
    validations: {
      validateCallback: (value) => value.length > 5 && value.length < 20,
      helperText: constants.emailErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "country",
    placeholder: "Country",
    type: "text",
    label: "Country",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "city",
    placeholder: "City",
    type: "text",
    label: "City",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "street",
    placeholder: "Street",
    type: "text",
    label: "Street",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.lengthErrorMessage,
    },
    variant: "outlined",
  },
  {
    key: "password",
    placeholder: "Password",
    type: "password",
    label: "Password",
    validations: {
      validateCallback: (value) => value.length > 2 && value.length < 20,
      helperText: constants.passwordErrorMessage,
    },
    variant: "outlined",
  },
]
export default function AddUserFields() {
  const classes = useStyles();
  const [form, setForm] = useState({
    first_name: "",
    last_name: "",
    email: "",
    role: "",
    country: "",
    city: "",
    street: "",
    password: "",
    is_deleted: "",
  });
  const [errors, setErrors] = useState({
    first_name: "",
    last_name: "",
    email: "",
    country: "",
    city: "",
    street: "",
    password: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    const values = Object.values(form).filter(e => e !== "")
    console.log(values)
    if(values.length === 0){
      alert("Please fill in all fields!")
    }
    
    createUser({
      first_name: form.first_name,
      last_name: form.last_name,
      email: form.email,
      role: form.role,
      country: form.country,
      city: form.city,
      street: form.street,
      password: form.password,
      is_deleted: form.is_deleted,
    });
    setForm({
      first_name: "",
      last_name: "",
      email: "",
      role: "",
      country: "",
      city: "",
      street: "",
      password: "",
      is_deleted: "",
    })
  };

  const onChangeHandler = (event, key) => {
    setForm({ ...form, [key]: event.target.value });
  };
console.log(form)
  return (
    <div>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="Name"
            value={form.first_name}
            onChange={(event) => onChangeHandler(event, "first_name")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="Family"
            value={form.last_name}
            onChange={(event) => onChangeHandler(event, "last_name")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
          //Fix email validation
            type="email"
            variant="outlined"
            label="Email"
            value={form.email}
            onChange={(event) => onChangeHandler(event, "email")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            //Change to combobox for roles!!!!!
            variant="outlined"
            label="Role"
            value={form.role}
            onChange={(event) => onChangeHandler(event, "role")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="Country"
            value={form.country}
            onChange={(event) => onChangeHandler(event, "country")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="City"
            value={form.city}
            onChange={(event) => onChangeHandler(event, "city")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="Address"
            value={form.street}
            onChange={(event) => onChangeHandler(event, "street")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            variant="outlined"
            label="Password"
            type="password"
            value={form.password}
            onChange={(event) => onChangeHandler(event, "password")}
          />
        </FormControl>
        <Button
          style={{ height: 52, width: 100 }}
          type="submit"
          variant="contained"
          color="primary"
          onSubmit={handleSubmit}
        >
          Add
        </Button>
      </form>
    </div>
  );
}
