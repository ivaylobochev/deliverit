import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { Button, TextField } from "@material-ui/core";
import { createWarehouse } from "../../Requests/warehouses_requests";
import { useState } from "react";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function AddWarehouse(props) {
  const classes = useStyles();
  const [form, setForm] = useState({
    country: "",
    city: "",
    street: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    const values = Object.values(form).filter(e => e !== "")
    console.log(values)
    if(values.length === 0){
      alert("Please fill in all fields!")
    }
    createWarehouse({
      country: form.country,
      city: form.city,
      street: form.street,
    });

    setForm({
        country: "",
        city: "",
        street: "",
      
    })
  };

  const onChangeHandler = (event, key) => {
    setForm({ ...form, [key]: event.target.value });
  };

  return (
    <div>
      <form
        className={classes.root}
        noValidate
        autoComplete="off"
        onSubmit={handleSubmit}
      >
        <FormControl className={classes.margin}>
          <TextField
            value={form.country}
            variant="outlined"
            label="Country"
            onChange={(event) => onChangeHandler(event, "country")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            value={form.city}
            variant="outlined"
            label="City"
            onChange={(event) => onChangeHandler(event, "city")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            value={form.street}
            variant="outlined"
            label="Address"
            onChange={(event) => onChangeHandler(event, "street")}
          />
        </FormControl>
        <Button
          style={{ height: 52, width: 100 }}
          type="submit"
          variant="contained"
          color="primary"
          onSubmit={handleSubmit}
        >
          Add
        </Button>
      </form>
    </div>
  );
}
