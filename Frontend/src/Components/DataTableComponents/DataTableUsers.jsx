import * as React from "react";
import { DataGrid } from "@material-ui/data-grid";
import { useState, useEffect } from "react";
import { getUsers } from "../../Requests/user_requests";

import AddUserFields from "../AddUserFields/AddUserFields";
import { GridToolbar } from "@material-ui/data-grid";
import { employeeTableUsers } from "../../Common/dataTableColumns";
// import EditUserForm from "../EditForms/EditUsersForm";
// import { employeeTableWarehouse } from "../../Common/dataTableColumns";

// const columns = [
//   {
//     field: "",
//     headerName: "",
//     editable: false,
//     sortable: false,
//     filterable: false,
//     width: 120,
//     disableClickEventBubbling: true,
//     renderCell: (params) => {
//       return <EditUserForm {...params.row} />;
//     },
//   },
//   { field: "id", headerName: "ID", width: 96 },
//   {
//     field: "first_name",
//     headerName: "Name",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "last_name",
//     headerName: "Family",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "email",
//     headerName: "Email",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "role",
//     headerName: "Employee",
//     type: "boolean",
//     width: 140,
//     editable: true,
//   },
//   {
//     field: "country",
//     headerName: "Country",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "city",
//     headerName: "City",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "street",
//     headerName: "Street",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "is_deleted",
//     headerName: "Deleted",
//     type: "boolean",
//     width: 150,
//     editable: false,
//   },
// ];

const columns = employeeTableUsers;

export default function DataTableUsers() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers().then((data) => setUsers(data));
  }, []);

  return (
    <div>
      <AddUserFields />
    <br />
        <div style={{ height: "100%", width: "100%" }}>
          <DataGrid 
            style={{ height: 640, width: "100%" }}
            rows={users}
            columns={columns}
            pageSize={5}
            checkboxSelection
            disableSelectionOnClick
            components={{
              Toolbar: GridToolbar,
            }}
          />
          </div>
      </div>
  );
}
