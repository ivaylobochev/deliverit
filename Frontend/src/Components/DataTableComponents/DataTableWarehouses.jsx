import * as React from "react";
import { DataGrid } from "@material-ui/data-grid";
import { getWarehouses } from "../../Requests/warehouses_requests";
import { useState } from "react";
import { useEffect } from "react";
// import EditWarehouseDialog from "../EditWarehouseDialog/EditWarehouseDialog";
import AddWarehouseFields from "../AddWarehouseFields/AddWarehouseFields";
import { GridToolbar } from "@material-ui/data-grid";
// import EditWarehouseForm from "../EditForms/EditWarehouseForm";
import { employeeTableWarehouse } from "../../Common/dataTableColumns";

// const columns = [
//   {
//     field: "",
//     headerName: "",
//     editable: false,
//     sortable: false,
//     filterable: false,
//     width: 120,
//     disableClickEventBubbling: true,
//     renderCell: (params) => {
//       return <EditWarehouseForm {...params.row} />;
//     },
//   },
//   { field: "id", headerName: "ID", width: 100 },
//   {
//     field: "country",
//     headerName: "Country",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "city",
//     headerName: "City",
//     width: 150,
//     editable: false,
//   },
//   {
//     field: "address",
//     headerName: "Address",
//     width: 150,
//     editable: false,
//   },
// ];

const columns = employeeTableWarehouse;

export default function DataTableWarehouses() {
  const [warehouses, setWarehouses] = useState([]);
  useEffect(() => {
    getWarehouses().then((data) => setWarehouses(data));
  }, []);

  // const [rows, setRows] = React.useState(warehouses);
  // const handleCellEditCommit =
  //   ({ id, field, value }) => {
  //     if (field === "fullName") {
  //       const [country, city, street] = value.toString().split(" ");
  //       const updatedRows = rows.map((row) => {
  //         if (row.id === id) {
  //           return { ...row, country, city, street };
  //         }
  //         return row;
  //       });
  //       setRows(updatedRows);
  //     }
  //   };

  return (
    <div>
      <AddWarehouseFields />
      <br />
      <DataGrid
        style={{ height: 640, width: "100%" }}
        rows={warehouses}
        columns={columns}
        pageSize={5}
        checkboxSelection
        disableSelectionOnClick
        // onCellEditCommit={handleCellEditCommit}
        components={{
          Toolbar: GridToolbar,
        }}
      />
    </div>
  );
}
