import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { Button, TextField } from "@material-ui/core";
import { useState } from "react";
import { createParcel } from "../../Requests/parcels_requests";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function AddParcelFields() {
  const classes = useStyles();
  const [form, setForm] = useState({
    default_idwarehouse: "",
    weight: "",
    category: "",
    iduser: "",
    // to_warehouse: "",
    // idshipments: ""
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    const values = Object.values(form).filter(e => e !== "")
    console.log(values)
    if(values.length === 0){
      alert("Please fill in all fields!")
    }
    createParcel({
      default_idwarehouse: form.default_idwarehouse,
      weight: form.weight,
      category: form.category,
      iduser: form.iduser,
      // to_warehouse: form.to_warehouse,
      // idshipments: form.idshipments
    })

    setForm({
      default_idwarehouse: "",
      weight: "",
      category: "",
      iduser: "",
      // to_warehouse: "",
      // idshipments: ""
    })
  };

  const onChangeHandler = (event, key) => {
    // const element = form[key];
    // const newElement = { ...element, value: event.target.value };

    setForm({ ...form, [key]: event.target.value });
  };
  return (
    <div>
      <form
        className={classes.root}
        onSubmit={handleSubmit}
        noValidate
        autoComplete="off"
      >
        <FormControl className={classes.margin}>
          <TextField
            type="number"
            label="Warehouse ID"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.default_idwarehouse}
            onChange={(event) => onChangeHandler(event, "default_idwarehouse")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            type="number"
            label="Weight"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.weight}
            onChange={(event) => onChangeHandler(event, "weight")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            label="Category"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.category}
            onChange={(event) => onChangeHandler(event, "category")}
          />
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            type="number"
            label="User ID"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.iduser}
            onChange={(event) => onChangeHandler(event, "iduser")}
          />
        </FormControl>
        <Button
          style={{ height: 52, width: 100 }}
          type="submit"
          variant="contained"
          color="primary"
          onSubmit={handleSubmit}
        >
          Add
        </Button>
      </form>
    </div>
  );
}
