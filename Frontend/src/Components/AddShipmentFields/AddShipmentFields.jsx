import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import FormControl from "@material-ui/core/FormControl";
import { Button, FormHelperText, TextField } from "@material-ui/core";
import { useState } from "react";
import { createShipment } from "../../Requests/shipments_requests";

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));

export default function AddShipmentFields() {
  const classes = useStyles();

  const [form, setForm] = useState({
    status: "",
    from_idwarehouses: "",
    to_idwarehouses: "",
    departure: "",
    arrival: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();

    const values = Object.values(form).filter(e => e !== "")
    console.log(values)
    if(values.length === 0){
      alert("Please fill in all fields!")
    }
    createShipment({
      status: form.status,
      from_idwarehouses: form.from_idwarehouses,
      to_idwarehouses: form.to_idwarehouses,
      departure: form.departure,
      arrival: form.arrival,
    });

    setForm({
      status: "",
      from_idwarehouses: "",
      to_idwarehouses: "",
      departure: "",
      arrival: "",
    })
  };

  const onChangeHandler = (event, key) => {
    setForm({ ...form, [key]: event.target.value });
  };

  return (
    <div>
      <form
        className={classes.root}
        onSubmit={handleSubmit}
        noValidate
        autoComplete="off"
      >
        <FormControl className={classes.margin}>
          <TextField
            type="number"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.from_idwarehouses}
            onChange={(event) => onChangeHandler(event, "from_idwarehouses")}
          />
          <FormHelperText>From warehouse</FormHelperText>
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            type="number"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.to_idwarehouses}
            onChange={(event) => onChangeHandler(event, "to_idwarehouses")}
          />
          <FormHelperText>To warehouse</FormHelperText>
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            // label="Departure"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.departure}
            type="datetime-local"
            onChange={(event) => onChangeHandler(event, "departure")}
          />
          <FormHelperText>Departure</FormHelperText>
        </FormControl>
        <FormControl className={classes.margin}>
          <TextField
            // label="Arrival"
            variant="outlined"
            id="demo-customized-textbox"
            value={form.arrival}
            type="datetime-local"
            onChange={(event) => onChangeHandler(event, "arrival")}
          />
          <FormHelperText>Arrival</FormHelperText>
        </FormControl>
        <Button
          style={{ height: 52, width: 100 }}
          type="submit"
          variant="contained"
          color="primary"
          onSubmit={handleSubmit}
        >
          Add
        </Button>
      </form>
    </div>
  );
}
