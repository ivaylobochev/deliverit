import axiosInstance from "../axios";

/**
 * GET ALL PARCELS
 * @returns {array} filled with parcel info objects
 */
export const getParcels = async () => {
  const { data } = await axiosInstance.get("/parcels");

  const result = data.map((item) => {
    return {
      id: item.idparcels,
      default_idwarehouse: item.default_idwarehouse,
      weight: item.weight,
      category: item.category,
      iduser: item.iduser,
      to_warehouse: item.to_warehouse,
      idshipments: item.idshipments,
      shipment_status: item.shipments_status,
      is_deleted: item.is_deleted,
    };
  });

  return result;
};

export const updateParcel = async (id, body) => {
  const { data } = await axiosInstance.put(`/parcels/update/${id}`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

export const createParcel = async (body) => {
  const { data } = await axiosInstance.post(`/parcels/create`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

/**
 * GET ALL PARCELS
 * @returns {array} filled with parcel info objects
 */
 export const getUserParcels = async (id) => {
 
  const { data } = await axiosInstance.get(`/parcels/parcels_user/${id}`);

  const result = data.map((item) => {
    return {
      id: item.idparcels,
      default_idwarehouse: item.default_idwarehouse,
      weight: item.weight,
      category: item.category,
      iduser: item.iduser,
      to_warehouse: item.to_warehouse,
      idshipments: item.idshipments,
      shipment_status: item.shipments_status,
      is_deleted: item.is_deleted,
    };
  });

  return result;
};