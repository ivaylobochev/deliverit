import axiosInstance from "../axios";

/**
 * GET ALL SHIPMENTS
 * @returns {array} filled with shipment info objects
 */
export const getShipments = async () => {
  const { data } = await axiosInstance.get("/shipments");

  const result = data.map((item) => {
    return {
      id: item.idshipments,
      status: item.status,
      from_idwarehouses: item.from_idwarehouses,
      to_idwarehouses: item.to_idwarehouses,
      departure: item.departure,
      arrival: item.arrival,
      created_on: item.created_on,
    };
  });

  return result;
};

export const updateShipment = async (id, body) => {
  const { data } = await axiosInstance.put(`/shipments/update/${id}`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

export const createShipment = async (body) => {
  const { data } = await axiosInstance.post(`/shipments/create`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};
