import axiosInstance from "../axios";

/**
 * GET ALL USERS
 * @returns {array} filled with user info objects
 */
export const getUsers = async () => {
  const { data } = await axiosInstance.get("/users");

  const result = data.map((item) => {
    return {
      id: item.idusers,
      first_name: item.first_name,
      last_name: item.last_name,
      email: item.email,
      role: item.role,
      country: item.country,
      city: item.city,
      street: item.street,
      password: item.password,
      is_deleted: item.is_deleted,
      created_on: item.created_on,
    };
  });

  return result;
};

export const updateUser = async (id, body) => {
  const { data } = await axiosInstance.put(`/users/update/${id}`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

export const createUser = async (body) => {
  const { data } = await axiosInstance.post(`/users/register`, body);

  return data;
  // const { status } = await axiosInstance.post("/users/register", credentials);
  // if (status === 201) {
  //   return successfulRegistration;
};

export const logUser = async (body) => {
  const { data } = await axiosInstance.post(`/users/login`, body);

  return data.token;
};

export const logoutUser = async () => {
  const { data } = await axiosInstance.post(`/users/logout`);

  return data;
};
